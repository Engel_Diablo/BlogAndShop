<?php



/**
*@param string $filename имя файла без расширения
*@param string $path путь к директории куда фаил будет сохранен
*/
function upload($filename, $path = null)
{
    if(!$path){
       $path = dirname(__DIR__).'/upload';
    }
    
    if(!is_dir($path)){
        die(sprintf("%s не является директорией", $path));
    }

    $type = $_FILES['image']['type'];
    $tmpPath = $_FILES['image']['tmp_name'];
    $fullName = generateName($filename, $type);
    if(file_exists($tmpPath)){
        copy($tmpPath, $path.'/'.$fullName);
        return $fullName;
    }else{
        die(sprintf("не нашел фаил %s", $tmpPath));
    }
}

function generateName($name, $type)
{
    $map = [
        'image/jpeg' => '.jpg',
        'image/png' => '.png',
        ];
    return $name.$map[$type];
}