<?php
require_once('./library/driver.php');
$errors = [];
$article = find($_GET[id]);

if(!empty($_POST)){
    if (empty($_POST['title'])){
        $errors['title'] = "Поле не может быть пустым!";
    }
    if (empty($_POST['content'])){
        $errors['title'] = "Поле не может быть пустым!";
    }
    if(strlen($_POST['title']) > 225){
        $errors['title'] = "Имя не может быть больше 225 символов";
    } 
    if(empty($errors)){
        $article = $_POST;
        $article['id'] = uniqid();
        if(save($article)){
            header ("Location: http://blog/");
        }
    }
}

$page = './views/article.php';
$title = "Добавление статьи";