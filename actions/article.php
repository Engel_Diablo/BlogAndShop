<?php
require_once('./library/driver.php');
require_once('./library/fs.php');
$article = $_POST;
$errors = [];

upload(uniqid());

if(!empty($_POST)){
    if(empty($_POST['title'])){
        $errors['title'] = "Поле не должно быть пустым";
    }
    if(empty($_POST['content'])){
        $errors['content'] = "Поле не должно быть пустым";
    }
    if(strlen($_POST['title']) > 255){
        $errors['title'] = "Название не может иметь длину больше 255 символов";
    }
    if(empty($errors)) {
        $article = $_POST;
        $article['id'] = uniqid();
        $article['image'] = upload($article['id']);
        if(save($article)){
            header("Location: http://blog/");
        }
    }
}
        $page = './views/article.php';
        $title = "Добавление статьи";